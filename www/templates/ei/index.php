<?php
defined('_JEXEC') or die;
$app             = JFactory::getApplication();
$doc             = JFactory::getDocument();
$user            = JFactory::getUser();
$this->language  = $doc->language;
$this->direction = $doc->direction;
?>
<?php
$this->_scripts = $this->_script = array();
$document = JFactory::getDocument();
unset($this->_generator);
?>
<!DOCTYPE html>
<html>
<head>

  <link href="/images/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

  <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" type="text/css" />
  <script type="text/javascript" src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->baseurl ?>/js/map-infobox.min.js"></script>-->
  <script type="text/javascript" src="<?php echo $this->baseurl ?>/js/common.js"></script>
  <script type="text/javascript" src="<?php echo $this->baseurl ?>/js/js_pack.js"></script>
  <script type="text/javascript" src="<?php echo $this->baseurl ?>/js/jquery.magnific-popup.min.js"></script>
  <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
  <script type="text/javascript" src="<?php echo $this->baseurl ?>/js/mediabox.js"></script>
  <script type="text/javascript" src="<?php echo $this->baseurl ?>/js/jquery.animateNumber.min.js"></script>
  <script type="text/javascript" src="<?php echo $this->baseurl ?>/js/index.js"></script>
  <link rel="stylesheet" href="<?php echo $this->baseurl ?>/css/style.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo $this->baseurl ?>/css/home_maps.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo $this->baseurl ?>/css/homeServices.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo $this->baseurl ?>/css/homeProjects.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo $this->baseurl ?>/css/about.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo $this->baseurl ?>/css/homePartners.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo $this->baseurl ?>/css/popup.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo $this->baseurl ?>/css/contacts.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo $this->baseurl ?>/css/variables.css" type="text/css" />
  <link rel="stylesheet" href="<?php echo $this->baseurl ?>/css/spectrUslug.css" type="text/css" />
   <script src="http://code.jquery.com/jquery-migrate-1.0.0.js"></script>
  <jdoc:include type="head" />
  <!--[if lt IE 9]>
    <script src="<?php echo JUri::root(true); ?>/media/jui/js/html5.js"></script>
  <![endif]-->
</head>
<body class="home">

  <jdoc:include type="modules" name="banner" style="none" />
  <div class="wrapper-energo">
  <jdoc:include type="modules" name="topmenu" style="none" />
  </div>
  <div class="main-content">
	<jdoc:include type="modules" name="position-14" style="none" />
    <jdoc:include type="message" />
    <jdoc:include type="component" />
  </div>
  <jdoc:include type="modules" name="position-8" style="none" />
</body>
</html>