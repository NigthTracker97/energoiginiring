(function($)
{
    $(document).ready(function()
    {
        $('.form__request').on('submit',function (e) {
           e.preventDefault();
           $.post('/send.php', {fio:$('#inputFio')[0].value,tel:$('#inputTelef')[0].value, 
            email:$('#inputEmail')[0].value, question: $('#inputQuestion')[0].value  })
            .done(function() {
                jQuery('.form__request')[0].reset()
            })
            .fail(function() {
                alert( "Произошла ошибка, попробуйте повторить ваш запрос" );
            })
          return false
        })
        $('.home_projects__container__item_1_count_365').animateNumber({ number: 365 },2000);
        $('.home_projects__container__item_1_count_365_small').animateNumber({ number: 365 },2000);
        $('.home_projects__container__item_1_count_19').animateNumber({ number: 19 },2000);
        $('.home_projects__container__item_1_count_1081').animateNumber({ number: 1081 },2000);
        $('.popup-modal').magnificPopup({
            type: 'inline',
            preloader: false,
            closeBtnInside: true,
            showCloseBtn: true
        });
        $('.popup-modal-reviews').magnificPopup({
            type: 'inline',
            preloader: false,
            closeOnContentClick: true
        });

        $(document).on('click', '.request__content__button', function (e) {
            // e.preventDefault();
            $.magnificPopup.close();
        });
        ymaps.ready(init);
        var myMap;

        function init(){
            myMap = new ymaps.Map("contacts_map", {
                center: [60.945, 76.53],
                zoom: 14
            });
            myPlacemark = new ymaps.Placemark([60.94, 76.54], {
                hintContent: '<p class="ml20 mr20 mt10 mb10">ООО «Энерго Инжиниринг»<br/>' +
                'ХМАО, Нижневартовск, ул. Мира, 14п</p>'
            });

            myMap.geoObjects.add(myPlacemark);

            myPlacemark = new ymaps.Placemark([60.95, 76.53], {
                hintContent: '<p class="ml20 mr20 mt10 mb10">ООО «Энерго Инжиниринг»<br/>' +
                'ХМАО, Нижневартовск, ул. Индустриальная, 42а ст3"</p>'
            });

            myMap.geoObjects.add(myPlacemark);
        }
        
        $('.home_services__content__l_a_active').on('click',function () {
            var link = document.createElement('a');
            link.setAttribute('href','/prays/2.docx');
            link.setAttribute('download','Прайс цен на ремонт оборудования май 2017');
            onload=link.click();
            return false
        })

        $('.gallery-about_r__images').on('click',function () {
            var srcImage = 'images/galereyy/' + this.dataset.image
            $('.gallery-about_r__images > img').removeClass('active-images')
            $(this).find('img').addClass('active-images')
            $('.gallery-about_c__image').attr('src', srcImage)
        })
    })
})(jQuery);