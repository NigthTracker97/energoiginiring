var mobile_scale = 1;
var subload_bg_pano, subload_pano, pano_rotated, pano_loaded, toggleTourDescription, load_korpus, load_korpus_info, unload_korpus_info;
$(function () {
    var body_size = $('body');
    var centrer1 = $('#centrer1');
    var centrer2 = $('#centrer2');
    var scroll1 = centrer1.find('.scroll_frame');
    var scroll2 = centrer2.find('.scroll_frame');
    var preloader = $('#preloader_frame');
    var ratio = 0.5625;
    var apart_ratio = 1.364;
    var time = 700;
    var easyIn = 'easeInQuart';
    var easyOut = 'easeOutQuart';
    var easyInOut = 'easeInOutQuart';
    var ani = true;
    var mobile = false;
	var debug = false;
    var transitions_av = true;
    var pano_help = true;
    var page = body_size.attr('class');
    var old_page;
    var cur_pano = '01';
    var night = '';
    var frame_w, frame_h, frame_d, centrer1_w, centrer1_h, centrer1_t, centrer1_l, data, infra_xml, parallax, carousel, slideshow, sequence, route, mouse_pos, text_blur_bg, bg_pano, normal_pano, krpano, popup_video, big_ani_step, big_ani_group, scroll_frame, minimap, home_bg_timeout;
    var pages = [];
    var ani_names = [];
    var big_ani_names = {};
    var page_urls = {};
    var pano_ani_names = [];
    ani_names = ani_names.concat(pano_ani_names);
    $.each(big_ani_names, function (index, value) {
        ani_names.push(index);
    });
    var plans_val = {};
    plans_val['q'] = 1;

    if (!Modernizr.csstransitions || !Modernizr.cssanimations) {
        transitions_av = false;
        $.fn.transition = $.fn.animate;
        $.fn.transitionStop = $.fn.stop;
    }
    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB10|IEMobile|Opera Mini/.test(navigator.userAgent)) {
        mobile = true;
        $('#body_frame').attr('class', 'mobile');
        centrer2.append('<div class="rotate_help_frame"><img src="/images/rotate.png" class="rotate_help" /></div>');
        var rotate_help = centrer2.find('.rotate_help_frame');
    }
    $('.accel').css({'translate3d': 0});
    function content_move() {
        if (mobile) {
            var min_window_w = 1250;
            var min_window_h = 650;
            var min_window_d = min_window_h / min_window_w;
            var window_w = $(window).width();
            var window_h = $(window).height();
            var window_d = window_h / window_w;
            if (window_d < min_window_d) {
                mobile_scale = Math.min(1, window_h / min_window_h);
                body_size.css({'min-width': window_w / mobile_scale, 'min-height': min_window_h, 'transformOrigin': '0 0', 'scale': mobile_scale});
            } else {
                mobile_scale = Math.min(1, window_w / min_window_w);
                body_size.css({'min-width': min_window_w, 'min-height': window_h / mobile_scale, 'transformOrigin': '0 0', 'scale': mobile_scale});
            }
            if (rotate_help) {
                if (window_h > window_w) {
                    rotate_help.show();
                } else {
                    rotate_help.hide();
                }
            }
        }
        frame_w = body_size.width();
        frame_h = body_size.height();
        frame_d = frame_h / frame_w;
        if (frame_d > ratio) {
            centrer1_w = frame_h / ratio;
            centrer1_h = frame_h;
            centrer1_t = 0;
            centrer1_l = 0.5 * (frame_w - centrer1_w);
        } else {
            centrer1_w = frame_w;
            centrer1_h = frame_w * ratio;
            centrer1_t = 0.5 * (frame_h - centrer1_h);
            centrer1_l = 0;
        }
        element_check_size(centrer1);
        if (bg_pano) {
            element_check_size(bg_pano);
        }
        if (text_blur_bg) {
            move_blur_bg(text_blur_bg);
        }
        if (parallax) {
            parallax.reinitialise();
        }
    }
    function plan_check_size() {
        var fr = centrer2.find('.plan_frame_centrer:last');
        var fr2 = fr.parent();
        var plan_size = Math.min(fr2.width() / apart_ratio, fr2.height());
        fr.css({'width': plan_size * apart_ratio, 'height': plan_size, 'margin-top': -0.5 * plan_size, 'margin-left': -0.5 * plan_size * apart_ratio});
        if (svg_paper_2) {
            svg_paper_2.changeSize(plan_size * apart_ratio, plan_size);
            svg_paper_3.changeSize(plan_size * apart_ratio, plan_size);
        }
    }
    function move_blur_bg(text_bg) {
        text_bg.each(function () {
            var text_bg = $(this);
            var text_blur_bg = text_bg.find('.text_blur_bg');
            element_check_size(text_blur_bg);
            text_blur_bg.find('img').css({'left': -text_bg.offset().left / mobile_scale, 'top': -text_bg.offset().top / mobile_scale});
        })
    }
	
    function element_check_size(targ) {
        targ.css({'width': centrer1_w, 'height': centrer1_h, 'top': centrer1_t, 'left': centrer1_l});
    }
    $(window).on('resize', function () {
        content_move();
    });
    content_move();
    setTimeout(function () {
        content_move();
    }, 500);
    setTimeout(function () {
        content_move();
    }, 1500);
    setTimeout(function () {
        content_move();
    }, 2500);

    function load_js(targ, prev) {
        if (big_ani_step != null) {
            var big_ani_bg = $('.double_ani_frame .n' + big_ani_step);
            big_ani_bg.css({'opacity': 1});
            videoTime = setTimeout(function () {
                big_ani_step++;
                load_bg_video('fly/' + big_ani_group[big_ani_step], big_ani_bg);
                if (big_ani_step == big_ani_group.length - 1) {
                    big_ani_step = null;
                }
            }, video_hide_delay);
        } else {
            body_size.attr('class', targ);
            var frame1 = scroll1.find('.' + targ + '_frame:last');
            var frame2 = scroll2.find('.' + targ + '_frame:last');
            var prev_frame1 = frame1.prevAll();
            var prev_frame2 = frame2.prevAll();
            var dir;
            var prev_index = $.inArray(prev, pages);
            var next_index = $.inArray(targ, pages);
            function frame1_ani_ended() {
                scroll1.removeAttr('style');
                frame1.css({'left': 0});
                prev_frame1.remove();
            }
            function frame2_ani_ended() {
                scroll2.removeAttr('style');
                frame2.css({'left': 0});
                prev_frame2.remove();
            }
            if (prev_index != -1 && next_index != -1) {
                if (prev_index < next_index) {
                    dir = 1;
                } else {
                    dir = -1;
                }
            }
            var video_name = prev + '-' + targ;
            if (ani && $.inArray(video_name, ani_names) != -1) {
                video_transition_ended(frame1, frame2);
                prev_frame1.remove();
                prev_frame2.remove();
            } else
            if (dir) {
                frame1.css({'left': (100 * dir) + '%'});
                frame2.css({'left': (100 * dir) + '%'});
                var css = [];
                if (transitions_av) {
                    css[0] = {'translate3d': '0%'};
                    css[1] = {'translate3d': (-100 * dir) + '%'};
                } else {
                    css[0] = {'left': '0%'};
                    css[1] = {'left': (-100 * dir) + '%'};
                }
                scroll1.transitionStop(true).delay(100).css(css[0]).transition(css[1], time, easyInOut, function () {
                    frame1_ani_ended();
                });
                scroll2.transitionStop(true).delay(100 + time * 0.3).css(css[0]).transition(css[1], time, easyInOut, function () {
                    frame2_ani_ended();
                });
            } else {
                frame1.css({'left': 0, 'opacity': 0}).transition({'opacity': 1}, time, function () {
                    frame1_ani_ended();
                });
                frame2.css({'left': 0, 'opacity': 0}).transition({'opacity': 1}, time, function () {
                    frame2_ani_ended();
                });
                prev_frame1.transitionStop(true).transition({'opacity': 0}, time);
                prev_frame2.transitionStop(true).transition({'opacity': 0}, time);
            }
            load_page_js(targ, prev);
        }
    }

    $('.documents__list').jScrollPane({showArrows: false, autoReinitialise: true, verticalDragMinHeight: 30, verticalDragMaxHeight: 100});
    function load_page_js(targ, prev) {
        var frame1 = scroll1.find('.' + targ + '_frame:last');
        var frame2 = scroll2.find('.' + targ + '_frame:last');
        var menu_targ = targ;

        frame2.find('.text_scroll').removeClass('browser_scroll').jScrollPane({showArrows: false, autoReinitialise: true, verticalDragMinHeight: 30, verticalDragMaxHeight: 100});
        if (targ == 'home') {
            html_loaded = true;
            scroll_frame = new init_scroll($('.home_frame'),1000,0,true,{
                'callback' : function(num) {
                    frame2.find('.scroll_bullet[data-targ="' + num / 10 + '"]').addClass('active').siblings('.active').removeClass('active');
                    if(num == 60) {
                        $('.home__slogan.n6').addClass('hidden').transitionStop(true).delay(2000).transition({'top' : '20%', 'opacity' : 0}, 1000, function() {
                            $(this).hide();
                        });
                        $('.home__over, .shadow:last').addClass('hidden').transitionStop(true).delay(2000).transition({'opacity' : 0}, 500, function() {
                            $(this).hide();
                        });
                        $('.home__buttons').transitionStop(true).css({'display' : 'block', 'bottom' : '-10%', 'opacity' : 0}).delay(2300).transition({'bottom' : '10%', 'opacity' : 1}, 800);
                    } else {
                        $('.home__over.hidden, .shadow.hidden').removeClass('hidden').transitionStop(true).css({'display' : 'block', 'opacity' : 1});
                        $('.home__slogan.n6.hidden').removeClass('hidden').transitionStop(true).css({'display' : 'block', 'top' : '20%', 'opacity' : 0}).transition({'top' : '50%', 'opacity' : 1}, 500);
                    }
                    frame2.find('.nav[data-targ="' + num / 10 + '"]').addClass('active').siblings('.active').removeClass('active');
                    if(num == 60) {
                        $('.home__slogan.n6').addClass('hidden').transitionStop(true).delay(2000).transition({'top' : '20%', 'opacity' : 0}, 1000, function() {
                            $(this).hide();
                        });
                        $('.home__over, .shadow:last').addClass('hidden').transitionStop(true).delay(2000).transition({'opacity' : 0}, 500, function() {
                            $(this).hide();
                        });
                        $('.home__buttons').transitionStop(true).css({'display' : 'block', 'bottom' : '-10%', 'opacity' : 0}).delay(2300).transition({'bottom' : '10%', 'opacity' : 1}, 800);
                    } else {
                        $('.home__over.hidden, .shadow.hidden').removeClass('hidden').transitionStop(true).css({'display' : 'block', 'opacity' : 1});
                        $('.home__slogan.n6.hidden').removeClass('hidden').transitionStop(true).css({'display' : 'block', 'top' : '20%', 'opacity' : 0}).transition({'top' : '50%', 'opacity' : 1}, 500);
                    }
                    setSmileAnimation($('.web-animation-' + num), false);
                },

            });
            toggleMenu(false);
			if (location.search && location.search.startsWith('?i=')){
              scroll_frame.move(1500, null, parseInt(location.search.substr(3)));
            }
        } else
        if (targ == 'documents') {
            menu_targ = 'about';
            make_submenu_active(targ, frame2);
            load_page_video(targ);			
			frame2.find('.text_bg').moveEl({
				type:'from',
				opacity:0,
				'margin-left':250,
				left:'-25%',
				time:1000,
				delay:time,
				easing:easyInOut,
				callback:function(el){
					
				}
			});
        } else
        if (targ == 'parallax') {
            menu_targ = 'about';
            make_submenu_active(targ, frame2);
            parallax = frame1.find('.p_item').parallaxInit({
                transitions_av: transitions_av,
                mouse_pos: mouse_pos
            })
        } else
        if (targ == 'contacts') {
            frame2.find('.map_place').attr('id', 'map_place');
            load_contacts_map(frame2);
        }

        if(page != 'home') {
            toggleMenu(true);
        }

        frame1.find('.accel').css({'translate3d': 0});
        frame2.find('.accel').css({'translate3d': 0});
        if (frame2.find('.pano_place').length) {
            frame2.find('.pano_place')
				.css({'cursor': 'url(/images/cur1.cur), move'})
				.on('mousedown', function () {
					$(this).css({'cursor': 'url(/images/cur2.cur), move'})
				})
				.on('mouseup', function () {
					$(this).css({'cursor': 'url(/images/cur1.cur), move'})
				})
        }
        if (frame2.find('.text_blur_bg_frame').length) {
            text_blur_bg = frame2.find('.text_blur_bg_frame').parent();
            move_blur_bg(text_blur_bg);
        }

        menuActive(menu_targ);

        old_page = targ;
    }
	
    function unload_js(targ, next) {
        var frame1 = scroll1.find('.' + targ + '_frame:last');
        var frame2 = scroll2.find('.' + targ + '_frame:last');

        if (targ == 'home') {
            stop_intro();
        }

        route = unloadPlugin(route);
        param_search = unloadPlugin(param_search);
        carousel = unloadPlugin(carousel);
        sequence = unloadPlugin(sequence);
        parallax = unloadPlugin(parallax);
        slideshow = unloadPlugin(slideshow);
        if(scroll_frame) scroll_frame.remove();
        bg_pano = setNull(bg_pano);
        normal_pano = setNull(normal_pano);
        text_blur_bg = setNull(text_blur_bg);
        plans_val['f'] = setNull(plans_val['f']);
        minimap = setNull(minimap);

        if (frame2.find('.map_place').length) {
            frame2.find('.map_place').removeAttr('id');
        }
        if (frame2.find('.pano_place').length) {
            frame2.find('.pano_place').off();
        }
    }
    function unloadPlugin(targ) {
        if (targ) {
            targ.removeEvents();
            return null;
        }
    }
    function setNull(targ) {
        if (targ) {
            return null;
        }
    }
    function addZero(num) {
        return num < 10 ? '0' + num : num;
    }

    function menuActive(targ) {
        centrer2.find('.menu__link, .menu__item').removeClass('active').filter('[href$="/' + targ + '"]').addClass('active').parents('.menu__item').addClass('active');
        var menu_active = centrer2.find('.menu__item.active');
        if(!menu_active.hasClass('on-hover') && !menu_active.hasClass('hover')) {
            toggleSubmenu(menu_active, true);
        }

    }

    function toggleMenu(show) {
        if(show) {
            centrer2
                .addClass('menu-opened')
                .find('.menu__frame').addClass('visible').end()
                .find('.menu__button').addClass('active');
        } else {
            centrer2
                .removeClass('menu-opened')
                .find('.menu__frame').removeClass('visible').end()
                .find('.menu__button').removeClass('active');
        }
    }

    function toggleSubmenu(targ, show) {
        if(show) {
            targ.find('.menu__submenu').css({'display' : 'none', 'opacity' : 0}).stop(true).animate({'height' : 'show', 'opacity' : 1}, 500).end().siblings('.menu__item:not(.on-hover)').find('.menu__submenu').stop(true).animate({'height' : 'hide', 'opacity' : 0}, 500);
        } else {
            targ.find('.menu__submenu').stop(true).animate({'height' : 'hide', 'opacity' : 0}, 500).end().siblings('.menu__item.active:not(.on-hover)').find('.menu__submenu').css({'display' : 'none', 'opacity' : 0}).stop(true).animate({'height' : 'show', 'opacity' : 1}, 500);
        }
    }

    function toggleDocuments(show) {
        if(show) {
            centrer2
                .find('.documents__frame').css({'display' : 'block', 'left' : 0, 'opacity' : 0}).transition({'left' : 550, 'opacity' : 1}, 500, easyInOut).end()
                .find('.footer__docs-button').addClass('active');
        } else {
            centrer2
                .find('.documents__frame').transitionStop(true).transition({'left' : 0, 'opacity' : 0}, 500, easyInOut, function() { $(this).hide(); }).end()
                .find('.footer__docs-button').removeClass('active');
        }
    }

	function toggleAdvantagesFrame(show) {
        if(show) {
            centrer2
                .find('.advantages__frame_two').css({'display':'block', 'right':0, 'opacity':0.2}).transition({'right':'35%', 'opacity' : 1}, 500, easyInOut).end()
                .find('.advantages__close').css({'display':'none'}).transition({'display':'block'}, 500, easyInOut).end()
                .find('.advantages_two__button').css({'display':'block'}).transition({'display':'none'}, 500, easyInOut).end()
                .find('.advantages_two__button').addClass('active');
        } else {
            centrer2
                .find('.advantages__frame_two').transitionStop(true).transition({'right':0, 'opacity':0.2}, 500, easyInOut, function() { $(this).hide(); }).end()
                .find('.advantages__close').css({'display':'none'}).transition({'display':'block'}, 500, easyInOut).end()
                .find('.advantages_two__button').css({'display':'block'}).end()
                .find('.advantages_two__button').removeClass('active');
        }
    }

	function toggleTerritoryPointsLeft(show) {
        if(show) {
            centrer2
                .find('.territory-plate-left').transition({'display':'block', 'opacity':1}, 500, easyInOut).end()
                .find('.points__button_left').addClass('active');
        } else {
            centrer2
                .find('.territory-plate-left').transitionStop(true).transition({'display':'none', 'opacity':0}, 500, easyInOut, function() { $(this).hide(); }).end()
                .find('.points__button_left').removeClass('active');
        }
    }
	function toggleTerritoryPointsCenter(show) {
        if(show) {
            centrer2
                .find('.territory-plate-center').transition({'display':'block', 'opacity':1}, 500, easyInOut).end()
                .find('.points__button_center').addClass('active');
        } else {
            centrer2
                .find('.territory-plate-center').transitionStop(true).transition({'display':'none', 'opacity':0}, 500, easyInOut, function() { $(this).hide(); }).end()
                .find('.points__button_center').removeClass('active');
        }
    }
	function toggleTerritoryPointsRight(show) {
        if(show) {
            centrer2
                .find('.territory-plate-right').transition({'display':'block', 'opacity':1}, 500, easyInOut).end()
                .find('.points__button_right').addClass('active');
        } else {
            centrer2
                .find('.territory-plate-right').transitionStop(true).transition({'display':'none', 'opacity':0}, 500, easyInOut, function() { $(this).hide(); }).end()
                .find('.points__button_right').removeClass('active');
        }
    }
	function toggleLocationPage(show) {
        if(show) {
            centrer2
                .find('.location-cont').transition({'left':'-900px'}, 500, easyInOut).end()
                .find('.infra-button').transition({'left':0, 'margin-left':0}, 500, easyInOut).end()
                .find('.location-navi').transition({'display':'block', 'bottom':0, 'opacity':1}, 500, easyInOut).end()
                .find('.location_page__close').transition({'display':'block', 'opacity':1}, 500, easyInOut).end()
                .find('.location_page__button').addClass('active');
        } else {
            centrer2
                .find('.location-cont').transitionStop(true).transition({'left':'50%', 'margin-left':'-251px'}, 500, easyInOut).end()
                .find('.infra-button').transitionStop(true).transition({'left':'50%', 'margin-left':'-377px'}, 500, easyInOut).end()
                .find('.location-navi').transitionStop(true).transition({'bottom':'-500px', 'opacity':0}, 500, easyInOut).end()
                .find('.location_page__close').transitionStop(true).transition({'opacity':0}, 500, easyInOut, function() { $(this).hide(); }).end()
                .find('.location_page__button').removeClass('active');
        }
    }
	function toggleLocationCoords(show) {
        if(show) {
            centrer2
                .find('.location-coords-one').transition({'left':'-900px', 'display':'none'}, 700, easyInOut).end()
                .find('.location-coords-two').css({'right':'-900px'}).transition({'display':'block', 'right':'90px', 'opacity':1}, 500, easyInOut).end()
                .find('.locations-coords__close').transition({'display':'block', 'opacity':1}, 500, easyInOut).end()
                .find('.locations-coords-button').addClass('active');
        } else {
            centrer2
                .find('.location-coords-one').transitionStop(true).transition({'left':0, 'display':'block'}, 800, easyInOut).end()
                .find('.location-coords-two').transitionStop(true).transition({'right':'-900px', 'opacity':0}, 0, easyInOut).end()
                .find('.locations-coords__close').transitionStop(true).transition({'opacity':0}, 500, easyInOut).end()
                .find('.locations-coords-button').removeClass('active');
        }
    }
	function toggleCompass(show) {
        if(show) {
            centrer2
                .find('.ap-start-container').css({'display':'block', 'left':0, 'opacity':0}).transition({'left':-900, 'opacity':0}, 500, easyInOut).end()
                .find('.ap-start-container__close').addClass('active');
        } else {
            centrer2
                .find('.ap-start-container').transitionStop(true).transition({'left':0, 'opacity':0}, 500, easyInOut, function() { $(this).hide(); }).end()
                .find('.ap-start-container__close').removeClass('active');
        }
    }
	function toggleMortgageStart(show) {
        if(show) {
            centrer2
                .find('.mortgage__frame_start').css({'display':'block', 'left':0, 'opacity':0}).transition({'left':0, 'opacity':1}, 500, easyInOut).end()
                .find('.mortgage_start__button').addClass('active');
        } else {
            centrer2
                .find('.mortgage__frame_start').transitionStop(true).transition({'left':0, 'opacity':0}, 500, easyInOut, function() { $(this).hide(); }).end()
                .find('.mortgage_start__button').removeClass('active');
        }
    }
	function toggleMortgageSteps(show) {
        if(show) {
            centrer2
                .find('.mortgage__frame_steps').css({'display':'block', 'left':0, 'opacity':0}).transition({'left':590, 'opacity':1}, 500, easyInOut).end()
                .find('.mortgage_steps__button').addClass('active');
        } else {
            centrer2
                .find('.mortgage__frame_steps').transitionStop(true).transition({'left':0, 'opacity':0}, 500, easyInOut, function() { $(this).hide(); }).end()
                .find('.mortgage_steps__button').removeClass('active');
        }
    }
	function toggleCallbackForm(show) {
        if(show) {
            centrer2
                .find('.callback__frame').css({'display':'block', 'left':0, 'opacity':1}).transition({'left':590, 'opacity':1}, 500, easyInOut).end()
                .find('.callback__button').addClass('active');
        } else {
            centrer2
                .find('.callback__frame').transitionStop(true).transition({'left':0, 'opacity':0}, 500, easyInOut, function() { $(this).hide(); }).end()
                .find('.callback__button').removeClass('active');
        }
    }
	function toggleMortgageDocs(show) {
        if(show) {
            centrer2
                .find('.mortgage__frame_docs').css({'display':'block', 'left':0, 'opacity':0}).transition({'left':0, 'opacity':1}, 500, easyInOut).end()
                .find('.mortgage_docs__button').addClass('active');
        } else {
            centrer2
                .find('.mortgage__frame_docs').transitionStop(true).transition({'left':0, 'opacity':0}, 500, easyInOut, function() { $(this).hide(); }).end()
                .find('.mortgage_docs__button').removeClass('active');
        }
    }
    function setSmileAnimation(el, instant) {
        var timeout = instant ? 0 : 1500;
        var ani_array = [
            {
                keyframes : [
                    {offset : 0, 'background-position' : '0px 0px'},
                    {offset : 1, 'background-position' : '-252px 0px'}
                ],
                timing : {
                    duration : 500,
                    delay : 1000,
                    steps: 5
                }
            },
            {
                keyframes : [
                    {offset : 0, 'background-position' : '0px 0px'},
                    {offset : 1, 'background-position' : '-252px 0px'}
                ],
                timing : {
                    duration : 2500,
                    delay : 0,
                    iterations : 1,
                    steps: 5
                }
            }
        ];
        el.each(function() {
            var that = $(this);
            var ani_num = $(this).data('ani-num');
//            if(ani_num == 'sprite') {
                var step = that.data('step'),
                    frames = that.data('frames');
                var i = 0;
                that.addClass('animating');
                that.css({'background-position' : '0 0'});
                var time = that.data('time') || 25;
                setTimeout(function() {
                    var spriteInterval = setInterval(function() {
                        if(i < frames) {
                            i += 1;
                            that.css({'background-position' : '-' + (i * step) + 'px 0'});
                        } else {
                            that.removeClass('animating');
                            clearInterval(spriteInterval);
                        }
                    }, time);
                }, timeout);
        });

    }

    function changeBg(path, name, time, callback) {
        centrer1.find('.bg_img:last').after('<img class="bg_img video-hide" style="display: none;" src="'+path+''+(mobile?'m/':'')+name+'.jpg" />');
        var img=centrer1.find('.bg_img:last');
        img.load(function(){
            $(this).unbind('load').css({'display' : 'block', 'opacity' : 0}).transition({'opacity' : 1},time,function(){
                $(this).prevAll('.bg_img:not(.blur_bg)').remove();
                if(callback) callback();
            });
        });
    }

    function test_json(targ_function) {
        if (!data) {
            var date = new Date();
            $.ajax({
                url: '/js/data.json?v=' + addZero(date.getDate()) + addZero(date.getMonth() + 1) + date.getFullYear(),
                dataType: 'json',
                success: function (response) {
                    data = response;
                    targ_function();
					if(debug) {
						var errors=[];
						var counter=0;
						var targ_counter=0;
						function test_end() {
							counter++;
							if(counter==targ_counter) {
								console.log('need ' + counter + ' images, errors: ', errors)
							}
						}
						$.each(data.apartments,function(index,value){
							targ_counter++;
						});
						$.each(data.apartments,function(index,value){
							var img = new Image();
							img.src = '/images/apts/'+index+'.png';
							img.onload = function(){
								test_end();
							};
							img.onerror = function(){
								errors.push(index);
								test_end();
							};
						})
					}
                }
            });
        } else {
            targ_function();
        }
    }

    function make_submenu_active(targ, frame) {
        frame.find('.' + targ + '_sublink').addClass('active');
    }

    function scale_show(targ, delay, time, callback) {
        targ.css({'opacity': 0, 'scale': 0.8}).delay(delay).transition({'opacity': 1, 'scale': 1}, time, callback);
    }

    function hide_element(el, css) {
        if (!el.data('hidden')) {
            el.transitionStop(true).data('hidden', true).transition(css, (old_page?time:0));
        }
    }
    function show_element(el, css) {
        if (el.data('hidden')) {
            el.transitionStop(true).data('hidden', false).transition(css, (old_page?time:0));
        }
    }

    function scroll_reint(frame) {
        if (frame.length) {
            var jsp = frame.data('jsp');
            jsp.scrollTo(0, 0);
            jsp.reinitialise();
        }
    }
    function scroll_load_html(scroll_frame, html, time) {
        scroll_frame.transitionStop(true).transition({'opacity': 0}, time, function () {
            scroll_frame.find('.jspPane>div').html(html);
            scroll_reint(scroll_frame);
            scroll_frame.transition({'opacity': 1}, time);
        });
    }

    /* поп-ап обратной связи и видео */

    function open_popup(options) {
        if (!$('.' + options.path + '_popup').length) {
            options = $.extend({
                time: 300,
                loadAnimate: function (targ) {
                    targ.children().css({'display': 'block', 'opacity': 0, 'top': '60%'}).transition({'opacity': 1, 'top': '50%'}, 500);
                },
                beforeOpen: function () {
                },
                frame: centrer2.find('.load_frame:last')
            }, options);
            options.frame.append('<div class="popup_overlay ' + options.path + '_popup"></div>')
                    .find('.' + options.path + '_popup').load('/assets/pages/popup/' + options.path + '.html', function () {
                $(this).css({'display': 'block', 'opacity': 0}).transition({'opacity': 1}, options.time, function () {
                    options.loadAnimate($(this));
                    options.beforeOpen($(this));
                })
            })
        }
    }

    function close_popup(options) {
        options = $.extend({
            time: 300,
            unloadAnimate: function (targ, callback) {
                targ.children().transitionStop(true).transition({'opacity': 0, 'top': '40%'}, 500, callback);
            },
            afterClose: function () {
            }
        }, options);
        options.unloadAnimate(options.target, function () {
            options.target.transition({'opacity': 0}, options.time, function () {
                options.afterClose(options.target);
                $(this).remove();
            })
        });
    }

    function stop_bg_video() {
        big_ani_step = null;
        if (ani) {
            clearInterval(videoInt);
            clearTimeout(videoTime);
            bg_video.pause();
        }
    }

    function big_ani_get_bg(callback) {
        scroll1.append('<div class="load_frame double_ani_frame"></div>');
        var fr = $('.double_ani_frame');
        var targ_num = big_ani_group.length - 1;
        var loaded_num = 0;
        function load_ajax_bg(i) {
            $.ajax({
                type: "GET",
                url: page_urls[big_ani_group[i].split('-')[1]],
                beforeSend: function (request) {
                    request.setRequestHeader('X-PJAX', 'true');
                },
                success: function (data) {
                    var txt1 = /to_centrer1">([\s\S]*)<\/div>\s*<div class="to_centrer2/.exec(data)[1];
                    var src = /bg_img.*src="(.*)"/.exec(txt1)[1];
                    var bg = '<img class="div_100 n' + i + '" src="' + src + '" />';
                    fr.append(bg)
                            .find('.n' + i).css({'opacity': 0}).load(function () {
                        loaded_num++;
                        if (loaded_num == targ_num) {
                            callback();
                        }
                    });
                }
            })
        }
        for (var i = 0; i < targ_num; i++) {
            load_ajax_bg(i);
        }
    }

    function init_scroll(frame,time,start,force3d,functions) {
        var scroll_point=start;
        var scroll_pos=0;
        var scroll_items=[];
        var points=[];
        var max_scroll;
        var scroll_av=true;
        if(!mobile) {
            $(document).bind('mousewheel',function(event,delta) {
                if(scroll_av) {
                    scroll_pos+=delta;
                    if(scroll_pos<=-2) {
                        trigger_scroll(1);
                    } else
                    if(scroll_pos>=2) {
                        trigger_scroll(-1);
                    }
                }
            });
        } else {
            function handleHammer(ev) {
                var pos_x=100*ev.gesture.deltaX/(frame_w*mobile_scale);
                var pos_y=100*ev.gesture.deltaY/(frame_h*mobile_scale);
                //console.log(pos)
                switch(ev.type) {
                    case 'pan':
                        if(pos_x<-30 || pos_y<-30) {
                            trigger_scroll(1);
                        } else
                        if(pos_y>30 || pos_y>30) {
                            trigger_scroll(-1);
                        }
                        break;
                    case 'swipeleft':
                        trigger_scroll(1);
                        break;
                    case 'swiperight':
                        trigger_scroll(-1);
                        break;
                    case 'swipeup':
                        trigger_scroll(1);
                        break;
                    case 'swipedown':
                        trigger_scroll(-1);
                        break;
                }
            }
            var slider=frame.hammer().on('pan swipeleft swiperight swipeup swipedown', handleHammer);
            slider.data('hammer').get('swipe').set({ direction: Hammer.DIRECTION_ALL });
        }
        $(document)
            .bind('keydown',function(e){
                if(scroll_av) {
                    var key=e.which;
                    if(key==40 || key==39) {
                        trigger_scroll(1);
                    } else
                    if(key==38 || key==37) {
                        trigger_scroll(-1);
                    }
                }
            });
        function trigger_scroll(delta) {
            scroll_pos=0;
            scroll_point+=delta;
            if(scroll_point>max_scroll) {
                scroll_point=max_scroll;
            } else
            if(scroll_point<0) {
                scroll_point=0;
            } else {
                scroll_av=false;
                scroll_frame.move(time,delta);
            }
        }
        function sort_array(a,b) {
            var param1=Number(a[0]);
            var param2=Number(b[0]);
            if (param1 > param2) return 1;
            else if (param1 < param2) return -1;
            else return 0;
        }
        function sort_array_num(a,b) {
            var param1=Number(a);
            var param2=Number(b);
            if (param1 > param2) return 1;
            else if (param1 < param2) return -1;
            else return 0;
        }
        frame.find('.scroll-item').each(function(){
            var scroll_item=$(this);
            var scroll_data=[];
            $.each($(this).data(),function(index,value){
                var key_frame=/pos(\d+)p/.exec(index);
                if(key_frame) {
                    key_frame=key_frame[1];
                    scroll_data.push([key_frame,value]);
                    if($.inArray(key_frame,points)==-1) {
                        points.push(key_frame);
                    }
                    max_scroll=Math.max(max_scroll,key_frame);
                }
            });
            scroll_data.sort(sort_array);
            scroll_item.scroll_data=scroll_data;
            scroll_items.push(scroll_item);
        });
        points.sort(sort_array_num);
        max_scroll=points.length-1;
        var scroll_points_txt='<div class="scroll_points_frame"><div>';
        for(var i=0; i<=max_scroll; i++) {
            scroll_points_txt+='<div class="scroll_point"></div>';
        }
        scroll_points_txt+='</div></div>';
        frame.filter(':last').append(scroll_points_txt);
        scroll_points_txt=null;

        this.move=function(time,delta,pos) {
            if(functions.beforeScroll) {
                functions.beforeScroll();
            }
            if(pos!=null) {
                scroll_pos=0;
                scroll_point=pos;
            }
            scroll_av=false;
            var cur_scroll=Number(points[scroll_point]);
            frame.find('.scroll_points_frame .scroll_point').eq(scroll_point).addClass('active').siblings().removeClass('active');
            $.each(scroll_items,function(index,value){
                var result_pos=0;
                $.each(value.scroll_data, function(index2,value2) {
                    if(cur_scroll==Number(value2[0]) || (!value.scroll_data[index2+1] && cur_scroll>Number(value2[0]))) {
                        result_pos=index2;
                        return false;
                    }
                });
                var targ_css=value.scroll_data[result_pos][1].split(';');
                if(targ_css[0]=='display:none') {
                    value.transitionStop(true).css({'display':'none'}).data({'pos':'hidden'});
                } else {
                    var myRe=/(.*):(-?\d+\.?\d*)(.*)/;
                    var new_css={};
                    var targ_css_length=targ_css.length-1;
                    for(var i=0; i<targ_css_length; i++) {
                        var targ_css_val=myRe.exec(targ_css[i]);
                        var css_type=targ_css_val[1];
                        var css_pos=Number(targ_css_val[2])+''+targ_css_val[3];
                        new_css[css_type]=css_pos;
                    }
                    var cur_pos=value.data('pos');
                    if(result_pos!=cur_pos) {
                        value.transitionStop(true).css({'display':'block'}).data({'pos':result_pos});
                        value.transition(new_css,time);
                    }
                }
            });
            setTimeout(function(){
                scroll_av=true;
            },time);

            if(functions) {
                $.each(functions,function(index,value){
                    if(cur_scroll+'-'+points[scroll_point-delta]==index || points[scroll_point-delta]+'-'+cur_scroll==index) {
                        value(delta);
                    } else
                    if(index == 'callback') {
                        value(cur_scroll);
                    }
                })
            }
        };

        this.remove=function() {
            $(document).unbind('mousewheel keydown');
            scroll_frame=null;
            if(slider) {
                slider.off('pan swipeleft swiperight swipeup swipedown');
            }
        };
        this.on=function() {
            scroll_av=true;
        };
        this.off=function() {
            scroll_av=false;
        };
        this.moveWithStep = function(step) {
            trigger_scroll(step);
        };
        this.move(0,1);
        return this;
    }

    body_size.pjax2({
        beforeSend: function (new_page) {
            preloader.show();
            unload_js(old_page, new_page);
            stop_bg_video();
            var video_name = old_page + '-' + new_page;
            var frame1 = scroll1.find('.' + old_page + '_frame');
            var frame2 = scroll2.find('.' + old_page + '_frame');
            var video_hide = frame1.children('.video-hide');
            if (ani && $.inArray(video_name, ani_names) != -1) {
                is_video = true;
                html_loaded = false;
                var bg_visible = false;
                function video_load_ready() {
                    video_hide.stop(true).transitionStop(true).transition({'opacity': 1}, 0.25 * time, function () {
                        if (bg_visible == false) {
                            bg_visible = true;
                            load_bg_video('fly/' + video_name, video_hide);
                        }
                    });
                    frame1.children(':not(.video-hide)').transitionStop(true).transition({'opacity': 0}, time, function () {
                        $(this).hide();
                    });
                    frame2.transitionStop(true).transition({'opacity': 0}, time, function () {
                        $(this).remove();
                    })
                }
                if ($.inArray(video_name, pano_ani_names) != -1) {
                    krpano.call('moveto(0,0,tween(easeInExpo,0.5));');
                    videoTime = setTimeout(function () {
                        video_load_ready();
                    }, 500);
                } else
                if (big_ani_names[video_name]) {
                    big_ani_step = 0;
                    big_ani_group = big_ani_names[video_name];
                    video_name = big_ani_group[big_ani_step];
                    big_ani_get_bg(function () {
                        video_load_ready();
                    });
                } else {
                    video_load_ready();
                }
            } else {
                is_video = false;
                video_hide.stop(true).transitionStop(true).transition({'opacity': 1}, 0.25 * time);
            }
        },
        success: function (data, type, url, slug, custom_type) {
            page = type;
            var txt1 = /to_centrer1">([\s\S]*)<\/div>\s*<div class="to_centrer2/.exec(data)[1];
            var txt2 = /to_centrer2">([\s\S]*)<\/div>\s*$/.exec(data)[1];
            if (mobile) {
                txt1 = txt1.replace(/(bg_img.*"\s.*)\/(\w*\.jpg"\s\/>)/, '$1/m/$2');
            }
            scroll1.append('<div class="load_frame ' + type + '_frame' + custom_type + '" style="left:100%;">' + txt1 + '</div>');
            scroll2.append('<div class="load_frame ' + type + '_frame' + custom_type + '" style="left:100%;">' + txt2 + '</div>');
            $('.load_frame.' + type + '_frame:last-child .bg_img:first').load(function () {
                $(this).off('load');
                if (is_video) {
                    html_loaded = true;
                } else {
                    preloader.hide();
                    load_js(type, old_page);
                }
            });
            add_stat(url);
        },
        success_function: load_js
    });


    body_size.on('click', function (e) {
        var targ_id = e.target.id;
        var targ_class = e.target.className;
        var targ = $(e.target);
        var targ_data = targ.data('targ');

        if (targ_class == 'ani_toggle') {
            ani = false;
            targ.addClass('off').text('вкл. анимацию');
        } else
        if (targ_class == 'ani_toggle off') {
            ani = true;
            targ.removeClass('off').text('выкл. анимацию');
        } else
        if(targ_class == 'menu__button') {
            toggleMenu(true);
        } else
        if (targ_class == 'pano_open_btn') {
            normal_pano = load_pano(targ_data, scroll2.find('.vtour_frame'), true);
        } else
        if (targ_class == 'close_btn pano_close') {
            unload_pano();
        } else
        if (targ_class == 'open_video') {
            if (slideshow) {
                slideshow.off();
            }
            open_popup({
                path: targ_data,
                beforeOpen: function (frame) {
                    popup_video = frame.popupVideoInit({
                        margin_w: 150,
                        margin_h: 150
                    });
                },
                loadAnimate: function (frame) {
                    frame.find('.video_popup_center').css({'display': 'block', 'opacity': 0, 'scale': 0.8}).transition({'opacity': 1, 'scale': 1}, 500);
                },
                time: 200,
                frame: centrer2
            })
        } else
        if (targ_class == 'close_btn video_close') {
            close_popup({
                target: targ.parents('.popup_overlay'),
                afterClose: function (frame) {
                    popup_video = unloadPlugin(popup_video);
                },
                unloadAnimate: function (frame, callback) {
                    frame.find('.video_popup_center').transition({'opacity': 0, 'scale': 1.2}, 500, callback);
                },
                time: 200
            });
            if (slideshow) {
                slideshow.on();
            }
        } else
        if (targ_class == 'footer__callback-button') {
            open_popup({
                path: 'feedback1',
                beforeOpen: function (frame) {
                    frame.find('input, textarea').placeholder().on('change', function () {
                        $(this).removeClass('error');
                    })
                            .filter('.phone_input').mask('+7 (999) 999-99-99');
                }
            })
        } else
        if(targ_class == 'footer__privacy-button') {
            open_popup({
                path: 'privacy-policy',
                beforeOpen: function (frame) {
                    frame.find('.text_scroll').jScrollPane({showArrows: false, autoReinitialise: true, verticalDragMinHeight: 30, verticalDragMaxHeight: 100});
                }
            });
        }
        if(targ_class == 'apart_act reserve_btn') {
            open_popup({
                path: 'feedback2',
                beforeOpen: function (frame) {
                    frame.find('input, textarea').placeholder().on('change', function () {
                        $(this).removeClass('error');
                    })
                        .filter('.phone_input').mask('+7 (999) 999-99-99').end()
                        .filter('.apart_input').val('Квартира №' + data.apartments[plans_val['n']].n);
                }
            })
        } else
        if (targ_class == 'close_btn feedback_close') {
            close_popup({
                target: targ.parents('.popup_overlay')
            })
        } else
        if (targ_class == 'send_btn') {
            var fr = targ.parents('.feedback_bg');
            fr.find('input, textarea').each(function () {
                var val = $(this).val();
                if (val == '' || val == $(this).attr('placeholder') || ($(this).hasClass('mail_input') && !/\S+@\S+\.\S+/.test(val))) {
                    $(this).addClass('error');
                }
            });
            if (!fr.find('.error').length) {
                var txt_name = fr.find('.feedback_input1').val();
                var txt_text = '';
                var txt_title = fr.find('.feedback_name').data('title');
                /** желательно передавать его разный в зависимости от типа сообщения */
                var message_type = fr.find('.feedback_name').data('title2');
                var contact = '';
                if(fr.find('.phone_input') && fr.find('.phone_input').val()) {
                    message_type = 'callme';
                    contact = fr.find('.phone_input').val();
                } else 
				if(fr.find('.mail_input') && fr.find('.mail_input').val()) {
					contact = fr.find('.mail_input').val();
				}
                
                targ.addClass('no-active');
                $.cookie('name', $.md5(txt_name), {expires: 1, path: '/'});
                fr.find('input, textarea').each(function () {
                    txt_text += $(this).data('title') + ': ' + $(this).val() + '\n';
                });
                /*if(fr.hasClass('n2')) {
                 txt_text+='\nКвартира: '+plans_val['n']; // для брони квартиры
                 }*/
                
                $.ajax({
                    url: '/feedbacks/send',
                    cache: false,
                    type: 'POST',
                    data: {type: message_type, contact: contact, name: txt_name, title: txt_title, text: txt_text},
                    success: function (data) {
                        if (data == 'ok') {
                            fr
                                    .find('.feedback_inputs').fadeOut(200).end()
                                    .find('.feedback_sended').delay(200).fadeIn(200).delay(5000).fadeOut(200, function () {
                                close_popup({
                                    target: fr.parents('.popup_overlay'),
                                    unloadAnimate: function (frame, callback) {
                                        frame.find('.feedback_bg').transition({'left': '60%', 'opacity': 0}, 500, callback);
                                    }
                                })
                            });
                        } else {
                            targ.removeClass('no-active');
                        }
                        ga('send', 'event', 'форма', 'отправка', 'заявка');
                    },
                    error: function () {
                        targ.removeClass('no-active');
                    }
                });
            }
        } else
        if (targ_class == 'rotate_help' || targ_class == 'rotate_help_frame') {
            rotate_help.hide();
            rotate_help = null;
        } else
        if (targ_class == 'plans_close floor_close') {
            close_floor();
        } else
        if (targ_class == 'plans_close apart_close') {
            load_floor(200);
        } else
        if (targ_class == 'sect_left active') {
            plans_val['s']--;
            load_floor(100);
        } else
        if (targ_class == 'sect_right active') {
            plans_val['s']++;
            load_floor(100);
        } else
        if (targ_class == 'floor_down active') {
            plans_val['f']--;
            load_floor(100);
        } else
        if (targ_class == 'floor_up active') {
            plans_val['f']++;
            load_floor(100);
        } else
        if (targ_class == 'rooms_sel' || targ_class == 'rooms_sel active') {
            targ.toggleClass('active').siblings('.active').removeClass('active');
            test_floors(scroll2.find('.korpus_frame:last'), data.floors);
        } else
        if (targ_class == 'apart_act pdf_btn') {
            $.cookie('phone', centrer2.find('.footer__phone').text(), {expires: 1, path: '/'});
            setTimeout(function() {
                window.open('/assets/php/pdf.php?id=' + targ_data, '_blank');
            }, 0);
        } else
        if (targ_class == 'fonts_sel') {
            targ.addClass('active').siblings().removeClass('active');
            $('#body_frame').attr({'style': 'font-family: "' + targ_data + '" !important;'});
        } else
        if (targ_class == 'skip_intro') {
            load_page_js(page, old_page);
        } else
        if (targ_class == 'text_slide_title') {
            targ.addClass('active').next().stop(true).css({'opacity': 0}).animate({'opacity': 1, 'height': 'show'}, 500);
            targ.parent().siblings('.text_slide').find('.active').trigger('click');
        } else
        if (targ_class == 'text_slide_title active') {
            targ.removeClass('active').next().stop(true).animate({'opacity': 0, 'height': 'hide'}, 500);
        } else
        if (targ_class == 'close_btn news_popup_close') {
            $('.news_popup').transitionStop(true).data('visible', false).transition({'y' : '100%', 'opacity': 0}, 800, function () {
                $(this).css({'display': 'none'});
            });
            centrer2.find('.news__list').transitionStop(true).delay(300).transition({'left' : 435}, 1000);
			$('.news_subpage_btns').hide();
            var url = targ_data;
            body_size.load_content(url, {'suppress_load': true});
            add_stat(url);
            $('.news_item.active').removeClass('active');
        } else
        if(targ_class == 'home__vertical-line-triangle') {
            scroll_frame.moveWithStep(1);
        } else
        if(targ_class == 'home__vertical-line-triangle up') {
            scroll_frame.moveWithStep(-1);
        } else
		if(targ_class=='banners_frame_close') {
			$('.banners_frame').fadeOut(500);
		} else
        if(targ_class == 'points__button_left') {
            toggleTerritoryPointsLeft(true);
        } else
		if(targ_class == 'points__button_center') {
            toggleTerritoryPointsCenter(true);
        } else
		if(targ_class == 'points__button_right') {
            toggleTerritoryPointsRight(true);
        } else
		if(targ_class == 'location_page__button') {
            toggleLocationPage(true);
        } else
		if(targ_class == 'locations-coords-button') {
            toggleLocationCoords(true);
        } else
		if(targ_class == 'ap-start-container__button') {
            toggleCompass(true);
        } else
		if(targ_class == 'mortgage_start__button') {
            toggleMortgageStart(true);
        } else
		if(targ_class == 'advantages_two__button') {
            toggleAdvantagesFrame(true);
        } else
		if(targ_class == 'callback__button') {
            toggleCallbackForm(true);
        } else
		if(targ_class == 'mortgage_steps__button') {
            toggleMortgageSteps(true);
        } else
        if(targ_class == 'mortgage_docs__button') {
            toggleMortgageDocs(true);
        } else
        if(targ_class == 'footer__docs-button') {
            toggleDocuments(true);
        } else
		if(targ_class == 'points__close close_btn') {
            toggleDocuments(false);
        } else
		if(targ_class == 'documents__close close_btn') {
            toggleDocuments(false);
        } else
        if(targ_class == 'points__close_left close_btn') {
            toggleTerritoryPointsLeft(false);
        } else
		if(targ_class == 'points__close_center close_btn') {
            toggleTerritoryPointsCenter(false);
        } else
		if(targ_class == 'points__close_right close_btn') {
            toggleTerritoryPointsRight(false);
        } else
		if(targ_class == 'location_page__close') {
            toggleLocationPage(false);
        } else
		if(targ_class == 'locations-coords__close') {
            toggleLocationCoords(false);
        } else
		if(targ_class == 'ap-start-container__close close_btn') {
            toggleCompass(false);
        } else
		if(targ_class == 'mortgage_start__close close_btn') {
            toggleMortgageStart(false);
        } else
		if(targ_class == 'mortgage_steps__close close_btn') {
            toggleMortgageSteps(false);
        } else
		if(targ_class == 'callback__close close_btn') {
            toggleCallbackForm(false);
        } else
		if(targ_class == 'advantages__close close_btn') {
            toggleAdvantagesFrame(false);
        } else
        if(targ_class == 'mortgage_docs__close close_btn') {
            toggleMortgageDocs(false);
        } else
        if(targ_class == 'home__right-content-close') {
            toggleSreda(targ.parent(), false);
        } else
        if(targ_class == 'home__repeat-video-button') {
            unload_js('home', 'home');
            showIntro(centrer1.find('.home_frame'), centrer2.find('.home_frame'));
        } else
        if(targ_class == 'tour__minimap-point') {
            cur_pano = targ_data;
            subload_pano(targ_data);
        } else
        if(targ_class == 'tour__time-button n1' || targ_class == 'tour__time-button n2') {
            if(targ_data == 'night') {
                night = 'night/';
            } else {
                night = '';
            }
            targ.addClass('active').siblings('.active').removeClass('active');
            subload_pano(cur_pano, 'KEEPVIEW');
        } else
        if(targ_class == 'tour__description-close') {
            toggleTourDescription(targ.parent().data('targ'), false);
        } else
        if(targ_class == 'close_btn around_close') {
            showAround(false);
        } else
        if(targ_class == 'apart__plan-arrow-right active') {
            targ.removeClass('active').siblings('.apart__plan-arrow-left').addClass('active');
            plans_val['floor_frame'].find('.plan_frame_centrer').transitionStop(true).transition({'opacity': 0}, 500, function () {
                load_apart_img(plans_val['floor_frame'].find('.plan_frame_centrer'), '/assets/images/apts/' + plans_val['n'] + '-v2', false, 500);
            });
        } else
        if(targ_class == 'apart__plan-arrow-left active') {
            targ.removeClass('active').siblings('.apart__plan-arrow-right').addClass('active');
            plans_val['floor_frame'].find('.plan_frame_centrer').transitionStop(true).transition({'opacity': 0}, 500, function () {
                load_apart_img(plans_val['floor_frame'].find('.plan_frame_centrer'), '/assets/images/apts/' + plans_val['n'], false, 500);
            });
        } else
        if(targ_class == 'video__pause-button') {
            targ.addClass('play');
            bg_video.pause();
        } else
        if(targ_class == 'video__pause-button play') {
            targ.removeClass('play');
            bg_video.play();
        }
    })
	.on('mousemove', function (e) {
		mouse_pos = e;
		if (parallax) {
			parallax.move(e);
		}
	})
	.on('click', '.news_item, .news_subpage_btn', function (e) {
		if (!$(this).hasClass('active')) {
			var url = $(this).attr('href');
			var news_popup = $('.news_popup');
			var news_scroll_frame = news_popup.find('.text_scroll');
            var template = $(this).data('targ');
			$('.news_item[href="'+url+'"]').addClass('active').siblings().removeClass('active');
			body_size.load_content(url, {'suppress_load': true});
			add_stat(url);
			$.ajax({
				url: '/' + template + '_load?url='+url,
				dataType: 'html',
				success: function (response) {
					if (!news_popup.data('visible')) {
						news_popup.transitionStop(true).css({'display': 'block', 'opacity': 0, 'y' : '100%'}).data('visible', true).delay(300).transition({'opacity': 1, 'y' : 0}, 800);
                        centrer2.find('.news__list').transitionStop(true).transition({'left' : '72%'}, 1000);
						scroll_load_html(news_scroll_frame, response, 0);
					} else {
						scroll_load_html(news_scroll_frame, response, 300);
					}
				}
			});
			$.ajax({
				url: '/news_siblings?url='+url,
				dataType: 'html',
				success: function (response2) {
					$('.news_subpage_btns').html(response2);
				}
			})
		}
		e.preventDefault();
	})
        .on('mouseenter', '.menu__button', function() {
            toggleMenu(true);
        })
        .on('mouseleave', '.menu__frame', function() {
            if(page == 'home') {
                toggleMenu(false);
            }
        })
	.on('mouseenter', '.korpus_popup',  function () {
		var alt = $(this).data('targ');
		if (svg_paper) {
			svg_paper.getByAlt(alt).attr({'opacity': 0.4});
            toggle_korpus_info(alt);
		}
	})
	.on('mouseleave', '.korpus_popup', function () {
		var alt = $(this).data('targ');
		if (svg_paper) {
			svg_paper.getByAlt(alt).attr({'opacity': 0});
		}
        toggle_korpus_info(false);
	})
	.on('click', '.construction_fancy', function () {
        var url = $(this).data('url');
        var path = $(this).data('path');
        $.ajax({
            url: url,
            dataType: 'json',
            success: function (response) {
                var arr = [];
                for (var i = 0; i < response.length; i++) {
                    arr.push({href: path + '/' + response[i], title: response[i].replace(/\.\w+$/, '')});
                }
                $.fancybox(arr, {type: 'image', padding: 0, margin: [75, 100, 40, 100]});
                arr = null;
            }
        });
    })
	.on('mouseenter', '.search_result_tab .active', function () {
		var pos = $(this).offset();
		$('.search_preview_frame').css({'display': 'block', 'left': pos.left / mobile_scale, 'top': pos.top / mobile_scale})
			.find('.search_preview').attr('src', '/assets/images/apts/' + $(this).data('targ') + '.png');
	})
	.on('mouseleave', '.search_result_tab .active', function () {
		$('.search_preview_frame').css({'display': 'none'})
	})
        .on('mouseenter', '.menu__item.on-hover', function() {
            toggleSubmenu($(this), true);
            $(this).addClass('hover');
        })
        .on('mouseleave', '.menu__item.on-hover', function() {
            toggleSubmenu($(this), false);
            $(this).removeClass('hover');
        })
        .on('mouseenter', '.home__vertical-line-image', function() {
            if(/web-animation/.test($(this).attr('class')) && !$(this).hasClass('animating')) {
                setSmileAnimation($(this), true);
            }
        })
        .on('click', '.scroll_bullet', function() {
            scroll_frame.move(1500, null, $(this).data('targ'));
        })
        .on('click', '.nav', function() {
            scroll_frame.move(1500, null, $(this).data('targ'));
        })
        .on('click', '.home__colored-text', function() {
            if(!$(this).hasClass('active')) {
                toggleSreda(centrer2.find('.home__right-content-frame.n' + $(this).data('targ')), true);
            } else {
                toggleSreda(centrer2.find('.home__right-content-frame.n' + $(this).data('targ')), false);
            }
        })
        .on('click', '.pano_help', function() {
            $(this).transitionStop(true).transition({'opacity' : 0}, 800, function() { $(this).remove(); });
        })
        .on('click', '.around__item:not(.active)', function() {
            showAround($(this).data('targ'));
        })
        .on('mouseenter', '.around__object', function() {
            $(this).transitionStop(true).css({'z-index' : 1}).transition({'scale' : 1.4}, 300);
        })
        .on('mouseleave', '.around__object', function() {
            $(this).transitionStop(true).css({'z-index' : 0}).transition({'scale' : 1}, 300);
        })
   
		$('.popup-with-move-anim').magnificPopup({
			type: 'inline',
			fixedContentPos: false,
			fixedBgPos: true,
			overflowY: 'auto',
			closeBtnInside: true,
			preloader: false,
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-slide-bottom'
		});
		
});