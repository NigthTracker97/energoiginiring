<?php
/**
* @title		Joombig Gallery Images module
* @website		http://www.joombig.com
* @copyright	Copyright (C) 2013 joombig.com. All rights reserved.
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/

// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
?>
<link rel="stylesheet" type="text/css" href="<?php echo $mosConfig_live_site; ?>/modules/mod_joombig_gallery_images/assets/css/joombig.demo.galleryimage.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $mosConfig_live_site; ?>/modules/mod_joombig_gallery_images/assets/css/joombig.style.galleryimage.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $mosConfig_live_site; ?>/modules/mod_joombig_gallery_images/assets/css/joombig.elastislide.galleryimage.css" />
<?php
$document->addStyleSheet( 'modules/mod_joombig_gallery_images/assets/css/joombig.elastislide.galleryimage.css' );
switch ($styles) {
  case 0:
	$document->addStyleDeclaration( '
		.es-carousel-wrapper{
			background: #101010;
		}
		.rg-view a{
			background:#464646 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/views.png) no-repeat top left;
			border:3px solid #464646;
		}
		.rg-view a.rg-view-selected{
			background-color:#6f6f6f;
			border-color:#6f6f6f;
		}
		.rg-image-nav a{
			background:#000 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/nav.png) no-repeat -20% 50%;
		}
		.rg-image-wrapper{
			background:transparent url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/black.png) repeat top left;
		}
	' );
    break;
  case 1:
	$document->addStyleDeclaration( '
		.es-carousel-wrapper{
			background: #9ACD32;
		}
		.rg-view a{
			background:#9ACD32 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/views.png) no-repeat top left;
			border:3px solid #9ACD32;
		}
		.rg-view a.rg-view-selected{
			background-color:#9ACD32;
			border-color:#9ACD32;
		}
		.rg-image-nav a{
			background:#9ACD32 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/nav.png) no-repeat -20% 50%;
		}
		.rg-image-wrapper{
			background:#9ACD32 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/black.png) repeat top left;
		}
	' );
    break;
  case 2:
	$document->addStyleDeclaration( '
		.es-carousel-wrapper{
			background: #DA70D6;
		}
		.rg-view a{
			background:#DA70D6 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/views.png) no-repeat top left;
			border:3px solid #DA70D6;
		}
		.rg-view a.rg-view-selected{
			background-color:#DA70D6;
			border-color:#DA70D6;
		}
		.rg-image-nav a{
			background:#DA70D6 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/nav.png) no-repeat -20% 50%;
		}
		.rg-image-wrapper{
			background:#DA70D6 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/black.png) repeat top left;
		}
	' );
    break;
  case 3:
	$document->addStyleDeclaration( '
		.es-carousel-wrapper{
			background: #A52A2A;
		}
		.rg-view a{
			background:#A52A2A url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/views.png) no-repeat top left;
			border:3px solid #A52A2A;
		}
		.rg-view a.rg-view-selected{
			background-color:#A52A2A;
			border-color:#A52A2A;
		}
		.rg-image-nav a{
			background:#A52A2A url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/nav.png) no-repeat -20% 50%;
		}
		.rg-image-wrapper{
			background:#A52A2A url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/black.png) repeat top left;
		}
	' );
    break;
  case 4:
	$document->addStyleDeclaration( '
		.es-carousel-wrapper{
			background: #FF69B4;
		}
		.rg-view a{
			background:#FF69B4 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/views.png) no-repeat top left;
			border:3px solid #FF69B4;
		}
		.rg-view a.rg-view-selected{
			background-color:#FF69B4;
			border-color:#FF69B4;
		}
		.rg-image-nav a{
			background:#FF69B4 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/nav.png) no-repeat -20% 50%;
		}
		.rg-image-wrapper{
			background:#FF69B4 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/black.png) repeat top left;
		}
	' );
    break;
  case 5:
	$document->addStyleDeclaration( '
		.es-carousel-wrapper{
			background: #228B22;
		}
		.rg-view a{
			background:#228B22 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/views.png) no-repeat top left;
			border:3px solid #228B22;
		}
		.rg-view a.rg-view-selected{
			background-color:#228B22;
			border-color:#228B22;
		}
		.rg-image-nav a{
			background:#228B22 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/nav.png) no-repeat -20% 50%;
		}
		.rg-image-wrapper{
			background:#228B22 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/black.png) repeat top left;
		}
	' );
    break;
  case 6:
	$document->addStyleDeclaration( '
		.es-carousel-wrapper{
			background: #FF1493;
		}
		.rg-view a{
			background:#FF1493 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/views.png) no-repeat top left;
			border:3px solid #FF1493;
		}
		.rg-view a.rg-view-selected{
			background-color:#FF1493;
			border-color:#FF1493;
		}
		.rg-image-nav a{
			background:#FF1493 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/nav.png) no-repeat -20% 50%;
		}
		.rg-image-wrapper{
			background:#FF1493 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/black.png) repeat top left;
		}
	' );
    break;
  case 7:
	$document->addStyleDeclaration( '
		.es-carousel-wrapper{
			background: #9400D3;
		}
		.rg-view a{
			background:#9400D3 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/views.png) no-repeat top left;
			border:3px solid #9400D3;
		}
		.rg-view a.rg-view-selected{
			background-color:#9400D3;
			border-color:#9400D3;
		}
		.rg-image-nav a{
			background:#9400D3 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/nav.png) no-repeat -20% 50%;
		}
		.rg-image-wrapper{
			background:#9400D3 url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/black.png) repeat top left;
		}
	' );
    break;
  case 8:
	$document->addStyleDeclaration( '
		.es-carousel-wrapper{
			background: #2F4F4F;
		}
		.rg-view a{
			background:#2F4F4F url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/views.png) no-repeat top left;
			border:3px solid #2F4F4F;
		}
		.rg-view a.rg-view-selected{
			background-color:#2F4F4F;
			border-color:#2F4F4F;
		}
		.rg-image-nav a{
			background:#2F4F4F url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/nav.png) no-repeat -20% 50%;
		}
		.rg-image-wrapper{
			background:#2F4F4F url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/black.png) repeat top left;
		}
	' );
    break;
  case 9:
	$document->addStyleDeclaration( '
		.es-carousel-wrapper{
			background: #D2B48C ;
		}
		.rg-view a{
			background:#D2B48C url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/views.png) no-repeat top left;
			border:3px solid #D2B48C;
		}
		.rg-view a.rg-view-selected{
			background-color:#D2B48C;
			border-color:#D2B48C;
		}
		.rg-image-nav a{
			background:#D2B48C url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/nav.png) no-repeat -20% 50%;
		}
		.rg-image-wrapper{
			background:#D2B48C url('.$mosConfig_live_site.'/modules/mod_joombig_gallery_images/assets/images/black.png) repeat top left;
		}
	' );
    break;
}
if (($width == "auto") && ($height == "auto"))
{
	$document->addStyleDeclaration( '
		.es-carousel-wrapper{
			width: auto;
			height:69px;
		}
	' );
}
else
{
	if ($width == "auto")
	{
		$document->addStyleDeclaration( '
			.es-carousel-wrapper{
				width: auto;
			}
		' );
	}
	else
	{
		$document->addStyleDeclaration( '
			.es-carousel-wrapper{
				width: '.$width.'px;
			}
		' );
	}
	if ($height == "auto")
	{
		$document->addStyleDeclaration( '
			.es-carousel-wrapper{
				height: 69px;
			}
		' );
	}
	else
	{
		$document->addStyleDeclaration( '
			.es-carousel-wrapper{
				height: 69pxpx;
			}
		' );
	}
}
$document->addStyleSheet( 'modules/mod_joombig_gallery_images/assets/css/joombig.style.galleryimage.css' );
$document->addStyleDeclaration( '
	.rg-view a{
		float: left;
	}
' );
if (($width == "auto") && ($height == "auto"))
{
	$document->addStyleDeclaration( '
		.joomgig_container .rg-gallery{
			width: auto;
			height: auto;
		}
		.rg-image-wrapper{
			width: auto;
			height:auto;
		}
		.rg-view{
			width: auto;
		}
	' );
}
else
{
	if ($width == "auto")
	{
		$document->addStyleDeclaration( '
			.joomgig_container .rg-gallery{
				width: auto;
			}
			.rg-image-wrapper{
				width: auto;
			}
			.rg-view{
				width: auto;
			}
		' );
	}
	else
	{
		$document->addStyleDeclaration( '
			.joomgig_container .rg-gallery{
				width: '.$width.';
			}
			.rg-image-wrapper{
				width: 100%;
			}
			.rg-view{
				width: 100%;
			}
		' );
	}
	if ($height == "auto")
	{
		$document->addStyleDeclaration( '
			.rg-image-wrapper{
				height: auto;
			}
		' );
	}
	else
	{
		$document->addStyleDeclaration( '
			.rg-image-wrapper{
				height: '.$height.'px;
			}
		' );
	}
}
?>
<noscript>
	<style>
		.es-carousel ul{
			display:block;
		}
	</style>
</noscript>
<script id="img-wrapper-tmpl" type="text/x-jquery-tmpl">	
	<div class="rg-image-wrapper">
		{{if itemsCount > 1}}
			<div class="rg-image-nav">
				<a href="#" class="rg-image-nav-prev">Previous Image</a>
				<a href="#" class="rg-image-nav-next">Next Image</a>
			</div>
		{{/if}}
		<div class="rg-image"></div>
		<div class="rg-loading"></div>
		<?php if($caption == 1){?>
			<div class="rg-caption-wrapper">
				<div class="rg-caption" style="display:none;">
					<p></p>
				</div>
			</div>
		<?php }?>
	</div>
</script>
<style>
	.joomgig_container .rg-gallery{
		margin:0 auto;
	}
</style>
<div class="joomgig_container" style="margin-left:<?php echo $left;?>;">
	<div class="joombig_content">
		<div id="rg-gallery" class="rg-gallery">
			<div class="rg-thumbs" style="margin-left:3px;">
				<!-- Elastislide Carousel Thumbnail Viewer -->
				<div class="es-carousel-wrapper">
					<div class="es-nav">
						<span class="es-nav-prev">Previous</span>
						<span class="es-nav-next">Next</span>
					</div>
					<div class="es-carousel">
						<ul>
						<?php 
							$count1=1;
							foreach($lists as $item) { ?>
								<li><a href="#"><img src="<?php echo $item->image ?>" data-large="<?php echo $item->image ?>" alt="<?php echo $item->title ?>" data-description="<?php echo $count1;?>/<?php echo $items;?>" /></a></li>

						<?php 
							$count1++;
							} ?>	
						</ul>
					</div>
				</div>
				<!-- End Elastislide Carousel Thumbnail Viewer -->
			</div><!-- rg-thumbs -->
		</div><!-- rg-gallery -->
	</div><!-- content -->
</div><!-- container -->
<script>
jQuery.noConflict(); 
var call_mode = <?php echo $mode;?>;
</script>
<?php
if ($enable_jQuery == 1) {?>
	<script type="text/javascript" src="<?php echo $mosConfig_live_site; ?>/modules/mod_joombig_gallery_images/assets/script/jquery.min.js"></script>
<?php }?>
<script type="text/javascript" src="<?php echo $mosConfig_live_site; ?>/modules/mod_joombig_gallery_images/assets/script/jquery.tmpl.min.js"></script>
<script type="text/javascript" src="<?php echo $mosConfig_live_site; ?>/modules/mod_joombig_gallery_images/assets/script/jquery.elastislide.js"></script>
<script type="text/javascript" src="<?php echo $mosConfig_live_site; ?>/modules/mod_joombig_gallery_images/assets/script/gallery.js"></script>
