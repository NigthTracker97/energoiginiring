<?php
/**
* @title		Joombig Gallery Images module
* @website		http://www.joombig.com
* @copyright	Copyright (C) 2013 joombig.com. All rights reserved.
* @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
// Path assignments
$mosConfig_absolute_path = JPATH_SITE;
$mosConfig_live_site = JURI :: base();
if(substr($mosConfig_live_site, -1)=="/") { $mosConfig_live_site = substr($mosConfig_live_site, 0, -1); }

if (JVERSION < 3) {
	JHTML::_('behavior.mootools');
} else {
	JHtml::_('jquery.framework');        
}
// Include helper.php
if(!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
require_once (dirname(__FILE__).DS.'helper.php');
$lists 					= modjoombigImagesGalleryHelper::getList($params);
$uri 					= JURI::getInstance();
$uniqid					= $module->id;
$width					= $params->get('width', "700");
$height 				= $params->get('height', "auto");
$left 					= $params->get('left', "0%");
$layout					= $params->get('layout', 0);
switch ($layout) {
  case 0:
    $mode = 1;
	$caption = 1;
    break;
  case 1:
    $mode = 1;
	$caption = 2;
    break;
  case 2:
    $mode = 2;
	$caption = 1;
    break;
  case 3:
    $mode = 2;
	$caption = 2;
    break;
}
$styles					= $params->get('styles', 0);
$enable_jQuery			= $params->get('enable_jQuery', 1);
$items					= count($lists);
$document 				= JFactory::getDocument();
require(JModuleHelper::getLayoutPath('mod_joombig_gallery_images'));
?>
