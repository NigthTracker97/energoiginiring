ymaps.ready(init);
function init() {
	myMap = new ymaps.Map("jmap", {
      center: [y_cords.lat[0],y_cords.lng[0]],
        zoom: y_zoom,
		type: y_mapType
    });
	var length = y_cords.lat.length;
	for (var i = 0; i < length; i++) {
		myPlacemark = new ymaps.Placemark([y_cords.lat[i],y_cords.lng[i]], { 
			balloonContent: "" },{
			preset: y_preset,
			iconColor: y_cords.color[i]
		});
		myMap.geoObjects.add(myPlacemark);
	}
	myMap.behaviors.disable('scrollZoom')
	if (length > 1)
	{	
		myMap.setBounds(myMap.geoObjects.getBounds());
	}	
}