<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_google_map
 *
 * @copyright   Copyright (C) 2015 Artem Yegorov. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<script type="text/javascript">
var y_cords=  <?php echo $fields->marker ; ?>,
y_zoom  =  <?php echo (isset($fields->zoom) ? $fields->zoom : '16');?>,
y_mapType  = "yandex#" + "<?php echo (isset($fields->type) ? $fields->type : 'map'); ?>",
y_preset =  "islands#" + "<?php echo (isset($fields->icontype) ? $fields->icontype : 'dotIcon');?>";
</script>
<script type="text/javascript" src="modules/mod_yandex_map/js/joomly_map.js"></script>
<div class="joomly-map" style="max-width: <?php echo (isset($fields->width) ? $fields->width : 600)."px"; ?>;height: <?php echo (isset($fields->height) ? $fields->height : 400)."px"; ?>;
								margin-left: <?php echo isset($fields->margin) ? $fields->margin : "none"; ?>;margin-right: <?php echo isset($fields->margin) ? $fields->margin : "none"; ?>;">
	<div id="jmap" class="joomly-ymap"></div>
</div>	
